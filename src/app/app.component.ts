import { Component } from '@angular/core';
import { lorem } from 'faker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sentence = lorem.sentence();
  enteredText = '';
  success = false;

  onInput(value: string) {
    this.enteredText = value;
    this.success = this.sentence === value;
  }

  compare(sentenceLetter: string, enteredLetter: string) {
    if(!enteredLetter) {
      return 'pending';
    }

    return sentenceLetter === enteredLetter ? 'correct' : 'incorrect';
  }

}
